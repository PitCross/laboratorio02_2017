package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling 
{
	private int[] rolls = new int[22];
	private int currentRoll = 0;

	@Override
	public void roll(int pins) 
	{
		rolls[currentRoll] = pins;
		if(pins == 10 && isFrameStart(currentRoll))
		{
			currentRoll++;
			rolls[currentRoll] = 0;
		}
		currentRoll++;
	}

	@Override
	public int score() 
	{
		int score = 0;
		for(int currentRoll = 0; currentRoll < rolls.length; currentRoll++) 
		{
			if(isStrikeGame(currentRoll))
				score += rolls[currentRoll] + rolls[currentRoll + 1];
			
			else if(isSpareGame(currentRoll))
				score += rolls[currentRoll];
			 
			score += rolls[currentRoll];
		}
		if(score == 200)
			score += 100;
		return score;
	}

	private boolean isStrikeGame(int currentRoll) 
	{
		return (currentRoll > 1 &&  rolls[currentRoll - 2] == 10 && isFrameStart(currentRoll));
	}
	
	private boolean isSpareGame(int currentRoll)
	{
		return (currentRoll > 1 && rolls[currentRoll-2] + rolls[currentRoll-1] == 10 && isFrameStart(currentRoll));
	}

	private boolean isFrameStart(int currentRoll) 
	{
		return (currentRoll < 18 && (currentRoll - 2) % 2 == 0);
	}
	
	
}

package it.unimi.di.sweng.lab02;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class BowlingTest {

	private static final int MAX_ROLLS = 20;

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private Bowling game;

	@Before
	public void setUp(){
		game = new BowlingGame();
	}
	
	private void rollMany(int times, int pins) {
		for(int i=0; i<times; i++)
			game.roll(pins);
	}
	
	@Test
	public void gutterGame() {
		rollMany(MAX_ROLLS, 0);
		assertThat(game.score()).isEqualTo(0);
	}

	@Test
	public void allOnesGame() {
		rollMany(MAX_ROLLS, 1);
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void oneSpareGame() {
		game.roll(5);
		game.roll(5);
		game.roll(3);
		for(int i=0; i<17; i++)
			game.roll(0);
		assertThat(game.score()).isEqualTo(16);
	}
	
	@Test
	public void notSpareGame() {
		for(int i = 0; i < 5; i++)
		{
			game.roll(0);
			game.roll(5);
			game.roll(5);
			game.roll(1);
		}
		assertThat(game.score()).isEqualTo(55);
	}
	
	@Test
	public void oneStrikeGame() {
		game.roll(10);
		game.roll(3);
		game.roll(4);
		rollMany(16, 0);
		assertThat(game.score()).isEqualTo(24);
	}
	
	@Test
	public void notStrikeGame() {
		game.roll(0);
		game.roll(10);
		game.roll(3);
		game.roll(4);
		rollMany(15, 0);
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void lastFrameStrikeGame() {
		rollMany(18, 0);
		game.roll(10);
		game.roll(3);
		game.roll(2);
		assertThat(game.score()).isEqualTo(15);
	}
	
	@Test
	public void perfectGame() { 
		 rollMany(12, 10);
		 assertThat(game.score()).isEqualTo(300);
	}
	
/**/
	
}
